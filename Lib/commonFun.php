<?php
if (get_magic_quotes_gpc ()) {

	function stripslashes_deep($value) {
		$value = is_array ( $value ) ? array_map ( 'stripslashes_deep', $value ) : stripslashes ( $value );
		return $value;
	}
	$_POST = array_map ( 'stripslashes_deep', $_POST );
	$_GET = array_map ( 'stripslashes_deep', $_GET );
	$_COOKIE = array_map ( 'stripslashes_deep', $_COOKIE );
}

/**
 * 裁剪字符串
 * @param unknown $str
 * @param unknown $len
 * @return string
 */
function  cutString($str,$len){
	$str=strip_tags($str);
	$strLen=strlen($str);
	//$str=$strLen.$str;
	if($strLen<=$len*3){
		return $str;
	}
	return mb_substr($str,0,$len,'utf-8')."...";
}

/**
 * 序列化
 * @param unknown $obj
 * @return string
 */
function my_serialize($obj) {
	return base64_encode ( gzcompress ( serialize ( $obj ) ) );
}

// 反序列化
/**
 * 反序列化
 * @param unknown $txt
 * @return mixed
 */
function my_unserialize($txt) {
	return unserialize ( gzuncompress ( base64_decode ( $txt ) ) );
}

/**
 * 获取参数
 *
 * @param unknown $key
 * @param unknown $df_value
 */
function request($key, $df_value = '') {
	return isset ( $_REQUEST [$key] ) ? trim ( $_REQUEST [$key] ) : $df_value;
}

/**
 * 获取参数，先get,后post,最后cookie
 * @param unknown $key 参数名
 * @return unknown|mixed|string
 */
function getParam($key){
	//$str="oetCntxEFGHI22Ko4CDqQCoxjAB";
	$val=request($key);
	if(empty($val)){
		$val=cookie($key);
	}
	return $val;
}

/**
 * 输出图片。根据需求自动裁剪图片
 *
 * @param unknown $file
 * @param number $width
 * @param number $height
 * @param string $def
 * @return unknown|mixed 使用例子：<img src="{:img('/wyzxqy/Tools/Upload/../../Upload/image/20150529/20150529103500_39141.jpg',90,90)}" />
 */
function img($file, $width = 200, $height = 200, $def = '') {



	if (preg_match ( '/^http:\/\//', $file )) {
		// 如果是远程文件
		return $file;
	}

	//获得文件扩展名
	$temp_arr = explode(".", $file);
	$file_ext = array_pop($temp_arr);
	$file_ext = trim($file_ext);
	$file_ext = strtolower($file_ext);

	$baseFile = basename ( $file ); // 找到文件名
	$basePath = str_replace ( $baseFile, "", $file ) . "temp/"; // 找到目录
	$baseFile = str_replace ( ".", "", $baseFile ); // 替换掉“.”
	$baseFile .= $width . "x" . $height . ".jpg";
	$basePath = str_replace ( C ( 'VIR_DIR' ), ".", $basePath ); // 替换掉虚拟目录
	if (! is_readable ( $basePath )) { // 判断文件夹是否存在，不存在就创建
		is_file ( $basePath ) or mkdir ( $basePath, 0777 );
	}
	$baseFile = $basePath . $baseFile;
	// trace ( $baseFile, "basefiel" );
	$file = str_replace ( C ( 'VIR_DIR' ), ".", $file ); // 替换掉虚拟目录
	if (! file_exists ( $file )) { // 判断原文件是否存在,不存在直接返回。
		if (empty ( $def )) { // 如果没有默认图片
			return $file;
		} else {
			// trace($def,"def");
			return $def;
		}
	}

	//后缀名
	if($file_ext=="gif"){
		return $file;
	}
	if (! file_exists ( $baseFile )) { // 判断文件是否存在
		$image = new \Think\Image ();
		$image->open ( $file ); // 生成一个缩放后填充大小的缩略图并保存
		$image->thumb ( $width, $height, \Think\Image::IMAGE_THUMB_FILLED )->save ( $baseFile ); // 生成缩略图
	}
	$str2 = substr ( $baseFile, 0, 2 ); // 取前两个字符串
	if ($str2 == "./") {
		$baseFile = C ( 'VIR_DIR' ) . substr ( $baseFile, 1 ); // 取前两个字符串
	}
	return $baseFile;
}




/**
 * 从摘要和内容中显示关键字
 * @param unknown $descript
 * @param unknown $content
 * @param unknown $keyword
 */
function getDescKey($descript,$content,$keyword){
	$startPre=80;//开始的位置
	$len=160;//长度
	//在摘要中找
	$index=intval( stripos($descript,$keyword));
	if($index>0){//如果摘要中就已经有关键字了。
		$begin = $index - $startPre > 0 ? $index - $startPre : 0;
		$descript = mb_strcut ( $descript, $begin, $len, "utf-8" ); // 视为字节流，utf－8下一个汉
		return  showKeyword($keyword,$descript);
	}
	$content=strip_tags($content);
	$index=intval( stripos($content,$keyword));
	if($index>0){//如果正文中就已经有关键字了。
		$begin=$index-$startPre>0?$index-$startPre:0;
		$content= mb_strcut($content,$begin,$len,"utf-8");//视为字节流，utf－8下一个汉
		return  showKeyword($keyword,$content);
	}
	return getDescript($descript,$content);//如果都没有关键字
	//echo intval( stripos($str,'斯'));
	//echo mb_strcut($str,4,10,"utf-8");//视为字节流，utf－8下一个汉
}



/**
 * 跳转
 *
 * @param unknown $url
 */
function jumpUrl($url) {
	if (! empty ( $url )) {
		redirect ( $url );
		exit ();
	}
}


function simpleHtmlEncode($str){
	$str = str_replace ( "\r\n", "<br/>", $str ); // \r\n
	$str= str_replace ( "\r", "<br/>", $str ); // 替换"\r"
	$str= str_replace ( "\n", "<br/>", $str ); // 替换"\n"
	$str= str_replace ( " ", "&nbsp;", $str ); // 替换"\n"
	return $str;
}


function simpleHtmlDecode($str){
	$str=str_replace("<br/>","\r\n",$str);
	$str=str_replace("&nbsp;"," ",$str);
	return $str;
}

/**
 * 显示关键字
 * @param unknown $keyword
 * @param unknown $content
 * @return mixed
 */
function showKeyword($keyword,$content){
	return str_replace($keyword,"<font class='red'>$keyword</font>",$content);
}


function  sendMail($mailto,$subject,$content){	
	vendor("Mail.smtp");
	$conditon['code']='email';
	$result=M('config')->where($conditon)->find();
	$model=unserialize($result['content']);
	//使用163邮箱服务器
	$smtpserver =$model['smtpserver'];// "smtp.qq.com";
	//163邮箱服务器端口
	$smtpserverport = 25;
	//你的163服务器邮箱账号
	$smtpusermail =$model['smtpusermail'];// "ycxxkj002@qq.com";
	//收件人邮箱
	$smtpemailto =$mailto;// "lzj500@qq.com";
	//你的邮箱账号(去掉@163.com)
	$smtpuser =$model['smtpuser'];// "2936890167";//SMTP服务器的用户帐号
	//你的邮箱密码
	$smtppass = $model['smtppass'];//"mail2015"; //SMTP服务器的用户密码
	//邮件主题
	$mailsubject = $subject;//"测试邮件发送";
	//邮件内容
	$mailbody =$content;// "<strong> PHP</strong>+<font style='color:red'>MySQL</font>+<a href='http://www.baidu.com'>点击跳转</a>";
	//邮件格式（HTML/TXT）,TXT为文本邮件
	$mailtype = "HTML";
	//这里面的一个true是表示使用身份验证,否则不使用身份验证.
	$smtp = new \smtp($smtpserver,$smtpserverport,true,$smtpuser,$smtppass);
	//是否显示发送的调试信息
	$smtp->debug =false;// TRUE;
	//$smtp->debug = TRUE;
	//发送邮件
	$res=$smtp->sendmail($smtpemailto, $smtpusermail, $mailsubject, $mailbody, $mailtype);
	
}

function get_url() {
	$sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
	$php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
	$path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
	$relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self.(isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : $path_info);
	return $sys_protocal.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '').$relate_url;
}


?>