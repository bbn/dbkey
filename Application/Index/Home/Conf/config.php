<?php
require_once  './Config/config.php';//引入共同配置
$localConfig=array(
		'SYS_NAME' => "丹霞山智能导游",	
		'WX_AUTH_URL'=>'http://wxcms.zhijianun.com/wxapi/wxapi.php?m=Home&c=Auth&a=auth_userinfo',		
		'WX_AUTH_BASE_URL'=>'http://wxcms.zhijianun.com/wxapi/wxapi.php?m=Home&c=Auth&a=auth_base',		
		'WX_JSSDK_URL'=>'http://wxcms.zhijianun.com/wxapi/wxapi.php?m=Home&c=Jssdk&a=getJssdk',
		'LOG_RECORD' => true, // 开启日志记录
		//'SHOW_PAGE_Trace' =>true,//打开调试
		'LOG_LEVEL'  =>'EMERG,ALERT,CRIT,ERR', // 只记录EMERG ALERT CRIT ERR 错误
		
		'PAGE_SIZE'=>15
);
//合并配置
foreach ($localConfig as $k=>$v){
	$config[$k]=$v;
}

unset($config['TMPL_ACTION_ERROR']);
unset($config['TMPL_ACTION_SUCCESS']);
/**
 *Tools'TMPL_ACTION_ERROR' => 'Public:error', // 默认成功跳转对应的模板文件
		'TMPL_ACTION_SUCCESS' => 'Public:success',
 */
return $config;