<?php

namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller {
	public function index() {
		$tables = M ( 'profiling' )->query ( "show databases" );
		// print_r($tables);
		$this->assign ( "tables", $tables );
		$this->display ();
	}
	public function table() {
		$t1 = microtime ( true );
		header ( "Content-type: text/html; charset=utf-8" );
		$table = request ( "table" );
		$this->assign ( "table", $table );
		$condition ['table_schema'] = $table;
		$where = "table_schema='" . $table . "'";
		$tables = M ( 'tables' )->where ( $where )->order ( "table_name asc" )->select ();	
		// 第一种方式
		$columns = M ( 'columns' )->where ( $where )->order ( "table_name asc,ordinal_position asc,column_name asc" )->select ();
		// print_r($columns);
		foreach ( $tables as $k => $v ) {
			$i=0;
			foreach ( $columns as $kk => $vv ) {
				
				if ($v ['table_name'] == $vv ['table_name']) {
					$i++;
					$tables [$k] ['column'] [$kk] = $vv;
					$tables [$k] ['column'] [$kk]['index']=$i;
					unset ( $columns [$kk] );
				}
			}
		}
// 		print_r($tables);
		// 第二种方式
		
		// print_r($columns);
// 		foreach ( $tables as $k => $v ) {
			
// 					$tables [$k] ['column'] =M ( 'columns' )->where (  "table_schema='" . $table . "' and table_name='".$v['table_name']."'" )->order ( "table_name asc,ordinal_position asc,column_name asc" )->select ();
			
// 		}
		// print_r($tables);
		$this->assign ( "tables", $tables );
		
		$this->display ();
		$t2 = microtime(true);
		echo '耗时'.round($t2-$t1,3).'秒';
	}
}